var surveyApp = angular.module('surveyApp', ['ui.router', 'ngMessages']);

surveyApp.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    
    $urlRouterProvider.otherwise('/login');

    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('login', {
            url: '/login',
            templateUrl: 'login.html',
            controller: 'loginController'
        })

        .state('register', {
        	url: '/register',
        	templateUrl: 'register.html',
        	controller: 'registerController'
        })
        
        
});

surveyApp.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
      });
    }
  }
}]);

surveyApp.directive('userNameValidate', ['Validator',  function(Validator) {
	return {
		require: "ngModel",
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.userNameValidate = function(username) {
	        	return Validator.usernameValidator(username);
	        };
		}
	}
}]);

surveyApp.directive('nameValidate', ['Validator',  function(Validator) {
	return {
		require: "ngModel",
		link: function(scope, element, attributes, ngModel) {
			ngModel.$validators.nameValidate = function(username) {
	        	return Validator.nameValidator(username);
	        };
		}
	}
}]);

surveyApp.directive("compareTo", [function() {
    return {
      require: "ngModel",
      scope: {
        otherModelValue: "=compareTo"
      },
      link: function(scope, element, attributes, ngModel) {

        ngModel.$validators.compareTo = function(modelValue) {
          return modelValue == scope.otherModelValue;
        };
      }
    };
 }]);