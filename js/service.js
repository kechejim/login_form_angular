surveyApp.factory('Validator', function() {

	usernameValidator = function(uname) {

		if(uname == null) return;
		var regexp = /^\w+(?:-\w+)?$/;
	 	var numexp = /^\d+(?:-\d+)?$/;
	 	var result = true;	 	

		if ((uname.search(regexp) == -1) || (uname.search(numexp) >= 0))
		    {		    	
		    	result = false; // username is invalid
		    }
		if((uname.match(/_/g) || []).length > 1) {

	    		result = false; // username could have 2 or more underscores.
	    	}
	    return result;
		
	}

	nameValidator = function(name) {
		if(name == null) return;
		var pattern = /^[a-zA-Z\s]*$/;  
		if (name.search(pattern) == -1)
			return false;
		return true;
	}

	return {
		usernameValidator: usernameValidator,
		nameValidator: nameValidator
	}

	
})