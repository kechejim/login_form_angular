surveyApp.value('Mainhost', 'http://feedbit-apiurl.rhcloud.com:80/fidbit/feedigit/business/user')

surveyApp.factory('authoApi', ['Mainhost', '$http', function(Mainhost, $http) {

   login = function(username, pass) {
      $http({method: 'POST', 
         url: Mainhost + 'login',
         data: {
            username: username,
            password: pass
         },
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
         }
      })
      .success(function(data, status) {
         console.log(data);
      })
      .error(function(data, status) {
         console.log('error');
      })
   }

   return {
      login: login
   }
   
}])