
surveyApp.value('Mainhost', 'http://feedbit-apiurl.rhcloud.com:80/fidbit/feedigit/business/user')

surveyApp.controller('loginController', [ '$http', 'Mainhost', '$scope',  function($http, Mainhost, $scope) {

	
	 $scope.submit = function() {

    	$http({method: 'POST', 
	         url: Mainhost + '/login',
	         data: {
	            userName: $scope.username,
	            password: $scope.password
	         }
	      })
	      .success(function(data, status) {
	         console.log(data);
	         if(data.statusMessage == "Login Successful") {
	         	alert("Welcome, Login is Successful!")
	         }
	         else {
	         	alert("Sorry, Wrong User Name or Password")
	         }
	      })
	      .error(function(data, status) {
	         console.log('error');
	      })
    }

}])

surveyApp.controller('registerController', ['$http', 'Mainhost', '$scope', function($http, Mainhost, $scope) {

	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 
    var today = yyyy+'-'+mm+'-'+dd;

    $scope.submit = function() {
	    $http({method: 'POST', 
	         url: Mainhost,
	         data: {
			  businessUserId: 0,
			  firstName: $scope.first_name,
			  lastName: $scope.last_name,
			  email: $scope.email,
			  phoneNumber: $scope.phoneNum,
			  loginDto: {
			    userName: $scope.username,
			    password: $scope.password1
			  },
			  isActive: 0,
			  lastUpdationDate: today,
			  companyName: $scope.company
			
	        }
	    })
	      .success(function(data, status) {
	         console.log(data);
	         if(data.status == "NOT_OK") {
	         	alert("Sorry, Wrong info")
	         }
	         else {
	         	alert("Welcome, You are successfully registered.")
	         }
	      })
	      .error(function(data, status) {
	         console.log('error');
	      })
	}


}])